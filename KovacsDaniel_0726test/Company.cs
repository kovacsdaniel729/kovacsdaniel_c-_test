﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KovacsDaniel_0726test
{
    abstract class Company
    {
        public string Name { get; private set; }
        public string Place { get; set; }

        public Company(string name)
        {
            Name = name;
        }

        public Company(string name, string place) : this(name)
        {
            Place = place;
        }

        public void WriteData()
        {
            Console.WriteLine("cégnév: {0}, telephely: {1}", Name, Place);
        }

        public abstract void WriteDescription();

    }
}
