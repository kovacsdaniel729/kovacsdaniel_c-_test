﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KovacsDaniel_0726test
{
    public static class ListExtension
    {
        public static T Second<T>( this List<T> list, Predicate<T> filter)
        {
            int count = 0;
            foreach (T item in list)
            {
                if (filter(item))
                {
                    count++;
                }
                if (count == 2)
                {
                    return item;
                }
            }
            throw new NoSecondElementException();
        }
    }
}
