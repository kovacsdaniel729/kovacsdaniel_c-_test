﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KovacsDaniel_0726test
{
    public delegate void PairPropGotEventHandler(int n);

    class Pair<T>
    {
        private T _item1;
        public T Item1 {
            get
            {
                if (PairPropGot != null)
                {
                    PairPropGot(1);
                }
                return _item1;
            }
            private set
            {
                _item1 = value;
            }
        }

        private T _item2;
        public T Item2
        {
            get
            {
                if (PairPropGot != null)
                {
                    PairPropGot(2);
                }
                return _item2;
            }
            private set
            {
                _item2 = value;
            }
        }

        public event PairPropGotEventHandler PairPropGot;

        public Pair(T item1, T item2)
        {
            Item1 = item1;
            Item2 = item2;
        }

    }
}
