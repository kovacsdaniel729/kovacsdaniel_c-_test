﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KovacsDaniel_0726test
{
    class ForProfitCompany : Company, IStock
    {
        public ForProfitCompany(string name) : base(name)
        {
        }

        public ForProfitCompany(string name, string place) : base(name, place)
        {
        }

        public void BuyStock()
        {
            Console.WriteLine("Részvényt veszek.");
        }

        public override void WriteDescription()
        {
            Console.WriteLine("Pénzt termelek a tulajdonosnak.");
        }
    }
}
