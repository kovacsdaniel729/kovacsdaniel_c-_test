﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KovacsDaniel_0726test
{
    class Program
    {
        static void Main(string[] args)
        {
            //listák
            Company[] companies = new Company[] {
                new NonProfitCompany("Holnap főzök anyámnak","Lajos utca 1."),
                new NonProfitCompany("Szeretem a szomszédom","Lajos utca 2."),
                new NonProfitCompany("Tegnap biciklivel mentem munkába","Lajos utca 2/a."),
                new ForProfitCompany("Nem adok visszajárót Bt.","Lajos utca 3."),
                new ForProfitCompany("Nem fizetek adót Kft.","Lajos utca 4."),
                new ForProfitCompany("Akik nem adnak borravalót Szövetkezet","Lajos utca 5."),
            };

            IStock[] stocks = new IStock[]
            {
                new Broker(),
                new ForProfitCompany("Kimentem a tőzsdére Rt.")
            };

            foreach (Company comp in companies)
            {
                comp.WriteData();
                comp.WriteDescription();
            }

            foreach (IStock item in stocks)
            {
                item.BuyStock();
            }

            //Pair
            Pair<string> pair = new Pair<string>("alfa","omega");


            //fájlkezelés
            Console.WriteLine("adjon meg egy elérési utat: ");
            string path = Console.ReadLine();

            try
            {
                if (File.Exists(path))
                {
                    ReadAndPrintFile(path);
                }
                else
                {
                    CreateFile(path);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }


            //Console.WriteLine("\n--- egyéb ---");
            //NemFeladatatCsakTeszteles();

            Console.WriteLine("\n--- end of program ---");
            Console.ReadKey();
        }

        public static void ReadAndPrintFile(string path)
        {
            using ( StreamReader sr = new StreamReader( new FileStream(path, FileMode.Open)))
            {
                while (!sr.EndOfStream)
                {
                    Console.WriteLine( sr.ReadLine() );
                }
            }
        }

        public static void CreateFile(string path)
        {
            using (StreamWriter sw = new StreamWriter(new FileStream(path, FileMode.Create)))
            {
                sw.WriteLine("létezett");
            }
        }

        public static void NemFeladatatCsakTeszteles()
        {
            //pair
            Pair<string> pair = new Pair<string>("egyeske", "ketteske");
            pair.PairPropGot += n => Console.WriteLine("elkértem az item{0}-t",n);

            Console.WriteLine("a pár elsö eleme: {0}", pair.Item1);
            Console.WriteLine("a pár második eleme: {0}", pair.Item2);

            //extension
            List<int> list = new List<int> { 1,2,3,4,5,6,7,8,9};
            Console.WriteLine(list.Second(n => n % 2 == 0));
            Console.WriteLine(list.Second(n => n > 5));
            try
            {
                Console.WriteLine(list.Second(n => n <1));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

    }
}
