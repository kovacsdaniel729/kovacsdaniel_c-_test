﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KovacsDaniel_0726test
{
    class NonProfitCompany : Company
    {
        public NonProfitCompany(string name) : base(name)
        {
        }

        public NonProfitCompany(string name, string place) : base(name, place)
        {
        }

        public override void WriteDescription()
        {
            Console.WriteLine("Segítek az embereken.");
        }
    }
}
