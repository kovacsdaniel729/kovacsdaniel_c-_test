﻿using System;
using System.Runtime.Serialization;

namespace KovacsDaniel_0726test
{
    [Serializable]
    internal class NoSecondElementException : Exception
    {
        public NoSecondElementException()
        {
        }

        public NoSecondElementException(string message) : base(message)
        {
        }

        public NoSecondElementException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoSecondElementException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}